<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        // auto load model
        $this->load->model('auth_model');

        $this->auth_model->check();
    }

	public function index()
	{
                $head['title'] = "Halaman Utama";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                $data['p_surat_masuk'] = $this->db->query("select count(*) as jumlah from tb_surat_masuk where status='pending'")->row()->jumlah;
                $data['p_surat_masuk'] = $this->db->query("select count(*) as jumlah from tb_surat_masuk where status='pending'")->row()->jumlah;
                $data['p_surat_keluar'] = $this->db->query("select count(*) as jumlah from tb_surat_keluar where status='pending'")->row()->jumlah;
                $data['p_surat_keluar'] = $this->db->query("select count(*) as jumlah from tb_surat_keluar where status='pending'")->row()->jumlah;

                $data['r_surat_masuk'] = $this->db->query("select count(*) as jumlah from tb_surat_masuk where status='return'")->row()->jumlah;
                $data['r_surat_masuk'] = $this->db->query("select count(*) as jumlah from tb_surat_masuk where status='return'")->row()->jumlah;
                $data['r_surat_keluar'] = $this->db->query("select count(*) as jumlah from tb_surat_keluar where status='return'")->row()->jumlah;
                $data['r_surat_keluar'] = $this->db->query("select count(*) as jumlah from tb_surat_keluar where status='return'")->row()->jumlah;

                $data['page'] = 'dashboard';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }


    public function kategori()
	{
                $head['title'] = "Kategori";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                $data['kategori'] = $this->db->query("select * from tb_kategori")->result();

                if(isset($_GET['delete'])){
                    if($this->deleteKategori($_GET['delete'])){
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                
                $data['page'] = 'kategori';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function tambah_kategori()
	{
                $head['title'] = "Tambah Surat Masuk";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                $data['foo'] = 'bar';

                $data['page'] = 'tambah_kategori';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }


    public function tambahKategori()
    {
        if(true){ // Hanya Untuk Pelanggan *di by pass
            $data = $this->input->post();

            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }

           if(isset($error)){
                $err = array_merge($err, $error);
           }
            if(!isset($err)){
                $query = $this->db->insert('tb_kategori', $data);
                if($query){
                    $this->session->set_flashdata('msg', 'sukses');
                    redirect('home/kategori');
                }else {
                    $this->session->set_flashdata('error', $insert['error']);
                    redirect('home/tambah_kategori?error='.$insert['error']);
                }
            }else {
                $err = implode(" ", $err);
                $this->session->set_flashdata('error', $err);
                redirect('home/tambah_kategori?error='.$err);
            }
        }else {
                $err = $error;
                $this->session->set_flashdata('error', $err);
                redirect('home/tambah_kategori?error='.$err);
        }
    }

    public function ubah_kategori()
	{
                $head['title'] = "Ubah Surat Masuk";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                $data['kategori'] = $this->db->query("select * from tb_kategori where id='".$_GET['id']."'")->result();

                $data['page'] = 'ubah_kategori';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }


    public function ubahKategori()
    {
        if(true){ // Hanya Untuk Pelanggan *di by pass
            $data = $this->input->post();
            $id = $data['id_kategori'];
            unset($data['id_kategori']);
            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }

           if(isset($error)){
                $err = array_merge($err, $error);
           }
            if(!isset($err)){
                $this->db->where("id", $id);
                $query = $this->db->update('tb_kategori', $data);
                if($query){
                    $this->session->set_flashdata('msg', 'sukses');
                    redirect('home/kategori');
                }else {
                    $this->session->set_flashdata('error', $insert['error']);
                    redirect('home/tambah_kategori?error='.$insert['error']);
                }
            }else {
                $err = implode(" ", $err);
                $this->session->set_flashdata('error', $err);
                redirect('home/tambah_kategori?error='.$err);
            }
        }else {
                $err = $error;
                $this->session->set_flashdata('error', $err);
                redirect('home/tambah_kategori?error='.$err);
        }
    }


    public function deleteKategori($id)
    {
        $query = $this->db->query("delete from `tb_kategori` where id = '$id'");
        if($query){
            return true;
        }else {
            return false;
        }
    }

    
    public function surat_masuk()
	{
                $head['title'] = "Surat Masuk";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                if($this->session->userdata("role") == "sekertaris") {
                    $data['surat_masuk'] = $this->db->query("select *, sm.status as status_surat, sm.id as id_surat_masuk from tb_surat_masuk sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori where sm.status='new' or sm.status='return'")->result();
                }else if($this->session->userdata("role") == "ketua") {
                    $data['surat_masuk'] = $this->db->query("select *, sm.status as status_surat, sm.id as id_surat_masuk from tb_surat_masuk sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori where sm.status='pending'")->result();
                }else {
                    $data['surat_masuk'] = $this->db->query("select *, sm.status as status_surat, sm.id as id_surat_masuk from tb_surat_masuk sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori")->result();
                }


                if(isset($_GET['show'])){
                    $this->load->view('modal_detail_surat');
                }

                if(isset($_GET['send'])){
                    if($this->setStatus("pending", "surat_masuk", $_GET['send'], $_GET['komentar'])){
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                
                if(isset($_GET['disposisi'])){
                    if($this->setStatus("accepted", "surat_masuk", $_GET['disposisi'], $_GET['komentar'])){
                        $id_surat = $this->db->query("select id_surat from tb_surat_masuk where id='".$_GET['disposisi']."'")->row()->id_surat;
                        $this->setStatus("saved", "surat", $id_surat, "");
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                

                if(isset($_GET['return'])){
                    if($this->setStatus("return", "surat_masuk", $_GET['return'], $_GET['komentar'])){
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                

                if(isset($_GET['delete'])){
                    if($this->deleteSurat("surat_masuk", $_GET['delete'])){
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                



                $data['page'] = 'surat_masuk';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function surat_keluar()
    {
                $head['title'] = "Surat Keluar";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                if($this->session->userdata("role") == "sekertaris") {
                    $data['surat_keluar'] = $this->db->query("select *, sm.status as status_surat, sm.id as id_surat_keluar from tb_surat_keluar sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori where sm.status='new' or sm.status='return'")->result();
                }else if($this->session->userdata("role") == "ketua") {
                    $data['surat_keluar'] = $this->db->query("select *, sm.status as status_surat, sm.id as id_surat_keluar from tb_surat_keluar sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori where sm.status='pending'")->result();
                }else {
                    $data['surat_keluar'] = $this->db->query("select *, sm.status as status_surat, sm.id as id_surat_keluar from tb_surat_keluar sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori")->result();
                }


                if(isset($_GET['show'])){
                    $this->load->view('modal_detail_surat');
                }

                if(isset($_GET['send'])){
                    if($this->setStatus("pending", "surat_keluar", $_GET['send'], $_GET['komentar'])){
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                
                if(isset($_GET['disposisi'])){
                    if($this->setStatus("accepted", "surat_keluar", $_GET['disposisi'], $_GET['komentar'])){
                        $id_surat = $this->db->query("select id_surat from tb_surat_keluar where id='".$_GET['disposisi']."'")->row()->id_surat;
                        $this->setStatus("saved", "surat", $id_surat, "");
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                

                if(isset($_GET['return'])){
                    if($this->setStatus("return", "surat_keluar", $_GET['return'], $_GET['komentar'])){
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                

                if(isset($_GET['delete'])){
                    if($this->deleteSurat("surat_keluar", $_GET['delete'])){
                        header("Location:?ok");
                    }else {
                        header("Location:?error");
                    }
                }
                



                $data['page'] = 'surat_keluar';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function daftar_surat()
    {
                $head['title'] = "Daftar Surat";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                if(isset($_GET['surat']) && $_GET['surat'] != 'Semua')
                {

                    $data['daftar_surat'] = $this->db->query("
                        select *, 'surat_". $_GET['surat'] ."' as `type`  from tb_surat join tb_kategori on tb_kategori.id = tb_surat.id_kategori join `tb_surat_". $_GET['surat'] ."` on `tb_surat_". $_GET['surat'] ."`.`id_surat`=`tb_surat`.`id` where tb_surat.status = 'saved'")->result();

                }else {    
                    $data['daftar_surat'] = $this->db->query("
                        select *, 'surat_masuk' as `type`  from tb_surat join tb_kategori on tb_kategori.id = tb_surat.id_kategori join `tb_surat_masuk` on `tb_surat_masuk`.`id_surat`=`tb_surat`.`id` where tb_surat.status = 'saved'
                        UNION
                        select *,  'surat_keluar' as `type`  from tb_surat join tb_kategori on tb_kategori.id = tb_surat.id_kategori join `tb_surat_keluar` on `tb_surat_keluar`.`id_surat`=`tb_surat`.`id` where tb_surat.status = 'saved'
                        ")->result();
                }

                if(isset($_GET['show'])){
                    $this->load->view('modal_detail_surat');
                }


                $data['page'] = 'daftar_surat';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function setStatus($status, $table, $id, $komentar)
    {
        if($komentar == "") 
        {
             $query = $this->db->query("update `tb_".$table."` set `status` = '$status' where id = '$id'");
        }else {
            $query = $this->db->query("update `tb_".$table."` set `status` = '$status',  `komentar` = '$komentar' where id = '$id'");
        }
        if($query){
            return true;
        }else {
            return false;
        }
    }

    public function deleteSurat($table, $id)
    {
        $data['id_surat'] = $this->db->query("select * from `tb_".$table."` where id = '$id'")->row()->id_surat;
        $query = $this->db->query("delete from `tb_".$table."` where id = '$id'");
        if($query){
            $this->db->query("delete from `tb_surat` where id = '$id_surat'");
            return true;
        }else {
            return false;
        }
    }
    
    public function tambah_surat_masuk()
	{
                $head['title'] = "Tambah Surat Masuk";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                $data['id_surat'] = $this->db->query("select * from tb_surat_masuk sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori")->result();
                $data['type_surat'] = 'masuk';

                $data['page'] = 'tambah_surat';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function tambah_surat_keluar()
    {
                $head['title'] = "Tambah Surat Keluar";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                $data['id_surat'] = $this->db->query("select * from tb_surat_keluar sm join tb_surat s on s.id = sm.id_surat join tb_kategori k on k.id = s.id_kategori")->result();
                $data['type_surat'] = 'keluar';

                $data['page'] = 'tambah_surat';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }
    
    public function tambahSurat()
    {
        if(true){ // Hanya Untuk Pelanggan *di by pass

            $type_surat = "surat_" . $this->input->post('type');
            $config['upload_path']          = './app-uploads/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
            $config['encrypt_name']         = true;
            $config['max_size']             = '1000'; //kilobyte
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('files'))
            {
                    $error = $this->upload->display_errors();
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
                    $file = $data['upload_data']['file_name'];
            }
            $data = $this->input->post();
            if(!is_array($data['no_surat'])) {
                $nomor_surat = $data['no_surat'];
            } else {
                $nomor_surat = implode("/", $data['no_surat']);
            }
            unset($data['files']);
            unset($data['no_surat']);
            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }
            if(isset($file)){
            $id_surat = $this->db->query("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'db_pengelolaan_surat' AND TABLE_NAME = 'tb_surat'")->result()[0]->AUTO_INCREMENT;
            $data = array('id_kategori' => $data['id_kategori'], 'time' => date("Y-m-d H:i:s"),
                            'file' => $file, 'status' => 'pending'
                );

            $data2 = array('id_surat' => $id_surat, 'nomor_surat' => $nomor_surat,
                            'status' => 'new'
                );


            
           
           if(isset($error)){
                $err = array_merge($err, $error);
           }
            if(!isset($err)){
                $query = $this->db->insert('tb_surat', $data);
                //print_r($insert);
                if($query){
                    $query = $this->db->insert('tb_' . $type_surat . '', $data2);
                    $this->session->set_flashdata('msg', 'sukses');
                    redirect('home/' . $type_surat . '');
                }else {
                    $this->session->set_flashdata('error', $insert['error']);
                    redirect('home/tambah_' . $type_surat . '?error='.$insert['error']);
                }
            }else {
                $err = implode(" ", $err);
                $this->session->set_flashdata('error', $err);
                redirect('home/tambah_' . $type_surat . 'error='.$err);
            }
            }else {
                $err = implode(" ", $err);
                $err = "Harap upload surat (".$error.")";
                $this->session->set_flashdata('error', $err);
                redirect('home/tambah_' . $type_surat . '?error='.$err);
            }
        }else {
                $err = $error;
                $this->session->set_flashdata('error', $err);
                redirect('home/tambah_' . $type_surat . '?error='.$err);
        }
    }

    public function laporan()
    {
                $head['title'] = "Laporan";
                $this->load->view('_templates/header', $head);

                $this->load->view('_templates/sidemenu');

                $data['page'] = 'laporan';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function print() {
        $data["bulan"] = date("M", strtotime(date("Y") . "-" . ($_GET['bulan']) . "-" . date("d")));
        $data["month"] = $_GET["bulan"];
        $data["tahun"] = $_GET['tahun'];
        $data['kategori'] = $this->db->query("select * from tb_kategori");
        
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan.pdf";
        $this->pdf->load_view('report_pdf', $data);

    }
}