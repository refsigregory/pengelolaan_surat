<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Tambah Kategori</b>
          </div> 
          <div class="panel-body"> <?=form_open_multipart('home/tambahKategori');?>
        <?php
            $error = $this->session->flashdata('error');
            if(isset($error)){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
        <?php } ?>

        <div class="form-group">
            <label>Nama Kategori:</label><br>
            <input type="text" name="nama_kategori" class="form-control" />
        </div>  

      <div class="form-group">
            <button class="btn btn-primary">Tambah</button>
      </div>
      </div>

</form>

</div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->