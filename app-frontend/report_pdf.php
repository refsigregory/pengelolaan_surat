<html>
<head>
<style type="text/css">
    body {
        font-size: 11pt;
        font-family: Arial;
    }
    h1 {
        padding: 0;
        margin: 0;
        font-size: 12pt;
    }

</style>
</head>
<body>
    <center><table width="100%"><tr><td align="center" width="20%"><img src="koperasi.png" width="100px" /></td><td align="left"><center><b>Laporan Pengelolaan Surat<br>Pusat Koperasi Unit Desa Sulawesi Utara</b></center></td></tr></table></center>
	<br>
	<center>
    Bulan: <?=$bulan;?> <?=$tahun;?>
    </center>
    
    <br>
    
    <table width="100%" border="1" cellspacing="0">
    	<tr>
    		<th colspan="2" align="">Surat Masuk</th>
	    	<tr>
	    		<th align="center">Kategori</th><th align="center">Jumlah Surat</th>
	    	</tr>

	    	<?php foreach($kategori->result() as $row):?>
	    	<tr>

	    		<td width="70%" valign="center" align="left"><?=$row->nama_kategori;?></td><td width="30%" valign="center" align="center"><?=$this->db->query("SELECT count(*) as jumlah from tb_surat join `tb_surat_masuk` on `tb_surat_masuk`.id_surat = `tb_surat`.id where id_kategori = '".$row->id."' AND year(`tb_surat`.time) = '$tahun' AND month(`tb_surat`.time) = '$month'")->row()->jumlah;?></td>
	    	</tr>
	    	<?php endforeach;?>
    	</tr>

    	
    	<tr>
    		<th colspan="2" align="">Surat Keluar</th>
    		<tr>
    		<th align="center">Kategori</th><th align="center">Jumlah Surat</th>
    	</tr>

	    	<?php foreach($kategori->result() as $row):?>
	    	<tr>
	    		<td width="70%" valign="center" align="left"><?=$row->nama_kategori;?></td><td width="30%" valign="center" align="center"><?=$this->db->query("SELECT count(*) as jumlah from tb_surat join `tb_surat_keluar` on `tb_surat_keluar`.id_surat = `tb_surat`.id where id_kategori = '".$row->id."' AND year(`tb_surat`.time) = '$tahun' AND month(`tb_surat`.time) = '$month'")->row()->jumlah;?></td>
	    	</tr>
	    	<?php endforeach;?>
    	</tr>

    </table>

    <br>
    <br>


</section>
</body>
</html>
