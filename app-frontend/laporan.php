<form action="<?=base_url('home/print');?>" target="_blank">
	<div class="form-group">
		<label>Bulan</label>:
		<?php
			$bulan = [
				"Januari",
				"Februari",
				"Maret",
				"April",
				"Januari",
				"Januari",
				"Januari",
				"Januari",
				"Januari",
				"Januari",
				"Januari",
			];
		?>
		<select name="bulan" class="">
			<?php for($i=0; $i < 12; $i++):?>
				<option value="<?=$i+1;?>"><?=date("M", strtotime(date("Y") . "-" . ($i+1) . "-" . date("d")));?></option>
			<?php endfor;?>
		</select>
	</div>
	<div class="form-group">
		<label>Tahun</label>:
		<select name="tahun" class="">
			<?php for($tahun = date("Y"); $tahun>2000; $tahun--):?>
				<option><?=$tahun;?></option>
			<?php endfor;?>
		</select>
	</div>
	<div class="form-group">
		<button class="btn btn-primary">Cetak Laporan</button>
	</div>
</form>