<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Surat Masuk</b>
          <?php if($this->session->userdata("role") == "sekretaris" || $this->session->userdata("role") == "admin") { ?>
          <a href="<?=site_url('home/tambah_surat_masuk');?>" class="btn btn-small btn-primary">Tambah</a>
          <?php } ?>
          </div> 
          <div class="panel-body">
         
<?php $no = 1; ?>
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead>
<tr>
    <th>No</th>
    <th>Nomor Surat</th>
    <th>Kategori</th>
    <th>Tanggal</th>
    <th>Status</th>
    <th>Pesan</th>
    <th>Aksi</th>
</tr>
</thead>
<tbody>
<?php if($surat_masuk != ""): ?>
<?php foreach ($surat_masuk as $row): ?>
<tr>
    <td><?=$no;?></td>
    <td><?=$row->nomor_surat;?></td>
    <td><?=$row->nama_kategori;?></td>
    <td><?=$row->time;?></td>
    <td><?=$row->status_surat;?></td>
    <td><?=$row->komentar;?></td>
    <td>
        <a href="<?=site_url('app-uploads/');?><?=$row->file;?>" target="_blank" class="btn btn-small btn-info">Unduh</a>

        <?php if($this->session->userdata("role") == "sekretaris" || $this->session->userdata("role") == "admin") { ?>
        <a href="<?=site_url('home/surat_masuk?delete=' . $row->id_surat_masuk);?>" class="btn btn-small btn-danger">Hapus</a>
        <?php } ?>
        
        <?php if($this->session->userdata("role") == "sekretaris" || $this->session->userdata("role") == "admin") { ?>
        <form>
            <input type="hidden" name="send" value="<?=$row->id_surat_masuk;?>">
            <div class="form-group">
                <input type="text" name="komentar" placeholder="Pesan">
            </div>
            <div class="form-group">
                <button class="btn btn-small btn-primary">Kirim</a>
            </div>
        </form>
        <?php } ?>
        
        <?php if($this->session->userdata("role") == "ketua" || $this->session->userdata("role") == "admin") { ?>
        <form>
            <input type="hidden" name="disposisi" value="<?=$row->id_surat_masuk;?>">
            <div class="form-group">
                <input type="text" name="komentar" placeholder="Pesan">
            </div>
            <div class="form-group">
                <button class="btn btn-small btn-success">Disposisi</a>
            </div>
        </form>
        <?php } ?>

        <?php if($this->session->userdata("role") == "ketua" || $this->session->userdata("role") == "admin") { ?>
        <form>
            <input type="hidden" name="return" value="<?=$row->id_surat_masuk;?>">
            <div class="form-group">
                <input type="text" name="komentar" placeholder="Pesan">
            </div>
            <div class="form-group">
                <button class="btn btn-small btn-warning">Kembalikan</a>
            </div>
        </form>
        <?php } ?>



    </td>
</tr>
<?php
$no++;
endforeach;
?>
<?php endif;?>
</tbody>
</table>
</div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "language": {
                    "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
                    "sProcessing":   "Sedang memproses...",
                    "sLengthMenu":   "Tampilkan _MENU_ entri",
                    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Cari:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                    }
                }
    });
    });
    function Tanya()
    {
        var tanya = confirm("Anda yakin Menghapus ini?");
        if(tanya == true)
            {
                return true;
            }else {
                return false;
            }
    }
</script>