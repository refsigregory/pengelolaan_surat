<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Daftar Masuk</b>
          	
          </div> 
          <div class="panel-body">
  <form>
          		<div class="form-group">
          			<div class="row">
          				<div class="col-xs-2">
          					<select class="form-control" name="surat">
	          					<option>Semua</option>
	          					<option value="masuk">Surat Masuk</option>
	          					<option value="keluar">Surat Keluar</option>
	          				</select>
          				</div>
          				<div class="col-xs-1">
          					<button class="btn btn-primary">Filter</button>
          				</div>
          			</div>
          		</div>
          	</form>      
<?php $no = 1; ?>
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead>
<tr>
    <th>No</th>
    <th>Tipe Surat</th>
    <th>Nomor Surat</th>
    <th>Kategori</th>
    <th>Tanggal</th>
    <th>Aksi</th>
</tr>
</thead>
<tbody>
<?php if($daftar_surat != ""): ?>
<?php foreach ($daftar_surat as $row): ?>
<tr>
    <td><?=$no;?></td>
    <td><?=($row->type=='surat_masuk') ? '<span class="label label-success">Surat Masuk</div>' : '<span class="label label-warning">Surat Keluar</span>';?></td>
    <td><?=$row->nomor_surat;?></td>
    <td><?=$row->nama_kategori;?></td>
    <td><?=$row->time;?></td>
    <td>
        <a href="<?=site_url('app-uploads/');?><?=$row->file;?>" target="_blank" class="btn btn-small btn-info">Download</a>
    </td>
</tr>
<?php
$no++;
endforeach;
?>
<?php endif;?>
</tbody>
</table>
</div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
    function Tanya()
    {
        var tanya = confirm("Anda yakin Menghapus ini?");
        if(tanya == true)
            {
                return true;
            }else {
                return false;
            }
    }
</script>