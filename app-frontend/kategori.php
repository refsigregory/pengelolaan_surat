<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Kategori</b>
          <?php if($this->session->userdata("role") == "sekretaris" || $this->session->userdata("role") == "admin") { ?>
          <a href="<?=site_url('home/tambah_kategori');?>" class="btn btn-small btn-primary">Tambah</a>
          <?php } ?>
          </div> 
          <div class="panel-body">
         
<?php $no = 1; ?>
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead>
<tr>
    <th>No</th>
    <th>Nama Kategori</th>
    <th>Aksi</th>
</tr>
</thead>
<tbody>
<?php if($kategori != ""): ?>
<?php foreach ($kategori as $row): ?>
<tr>
    <td><?=$no;?></td>
    <td><?=$row->nama_kategori;?></td>
    <td>

        <?php if($this->session->userdata("role") == "sekretaris" || $this->session->userdata("role") == "admin") { ?>
            <a href="<?=site_url('home/ubah_kategori?id=' . $row->id);?>" class="btn btn-small btn-warning">Ubah</a>
            <a href="<?=site_url('home/kategori?delete=' . $row->id);?>" class="btn btn-small btn-danger">Hapus</a>
        <?php } ?>

    </td>
</tr>
<?php
$no++;
endforeach;
?>
<?php endif;?>
</tbody>
</table>
</div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "language": {
                    "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
                    "sProcessing":   "Sedang memproses...",
                    "sLengthMenu":   "Tampilkan _MENU_ entri",
                    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Cari:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                    }
                }
    });
    });
    function Tanya()
    {
        var tanya = confirm("Anda yakin Menghapus ini?");
        if(tanya == true)
            {
                return true;
            }else {
                return false;
            }
    }
</script>