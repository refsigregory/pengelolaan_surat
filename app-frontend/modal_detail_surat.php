
<!-- Modal -->
<div class="modal fade" id="modalNewOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Kendaraan</h4>
      </div>
      <div class="modal-body">
        
        <div class="form-group">
          <div class="row">
            <div class="col-xs-3">Nomor Surat</div>
            <div class="col-xs-1">:</div>
            <div class="col-xs-8 text-left"> <?=$detail->nomor_surat;?></div>
          </div>

          <div class="row">
            <div class="col-xs-3">Kategori</div>
            <div class="col-xs-1">:</div>
            <div class="col-xs-8 text-left"> <?=$detail->nama_kategori;?></div>
          </div>

          <div class="row">
            <div class="col-xs-3">File</div>
            <div class="col-xs-1">:</div>
            <div class="col-xs-8 text-left"> <a href=""><?=$detail->tipe_mobil;?></div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#modalNewOrder").modal('show');
    $('#customSize').click(function(){
        var type = $('.ukuran-form').attr('data-type');
        changeSize(type);
    })
    function changeSize(type)
    {
      if(type=='select'){
        $('.ukuran').html('<input type="text" class="form-control" name="ukuran" data-type="textbox" placeholder="Masukan Ukuran" value="">');
      }else {
        $('.ukuran').html('<select name="ukuran" class="form-control ukuran-form" data-type="select"><option>small</option><option>medium</option><option>large</option></select>');
      }
    }
  });
</script>