<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Ubah Kategori</b>
          </div> 
          <div class="panel-body"> <?=form_open_multipart('home/ubahKategori');?>
          <input type="hidden" name="id_kategori" value="<?=$kategori[0]->id;?>"/>
        <?php
            $error = $this->session->flashdata('error');
            if(isset($error)){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
        <?php } ?>

        <div class="form-group">
            <label>Nama Kategori:</label><br>
            <input type="text" name="nama_kategori" class="form-control" value="<?=$kategori[0]->nama_kategori;?>"/>
        </div>  

      <div class="form-group">
            <button class="btn btn-primary">Ubah</button>
      </div>
      </div>

</form>

</div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->