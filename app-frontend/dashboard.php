
<div class="alert alert-info"><b>Selamat Datang  <?=ucfirst($this->session->userdata('username'));?> (<?=$this->session->userdata('role');?>)!</b></div>

<!--<img src="logo.png" width="100%" height="300" />-->

<?php if($this->session->userdata('role')=='ketua' || $this->session->userdata('role')=='admin'): ?>
		<?php if($p_surat_masuk>0):?>
		<div class="alert alert-warning"><b><?=$p_surat_masuk;?></b></a> Surat Masuk baru untuk Disposisi.</div>
	<?php endif; ?>

	<?php if($p_surat_keluar>0):?>
		<div class="alert alert-warning"><b><?=$p_surat_keluar;?></b></a> Surat Keluar baru untuk Disposisi.</div>
	<?php endif; ?>
<?php endif; ?>
<?php if($this->session->userdata('role')=='sekretaris' || $this->session->userdata('role')=='admin'): ?>
	<?php if($r_surat_masuk>0):?>
		<div class="alert alert-warning"><b><?=$r_surat_masuk;?></b></a> Surat Masuk yang dikembalikan untuk ditinjau ulang.</div>
	<?php endif; ?>

	<?php if($r_surat_keluar>0):?>
		<div class="alert alert-warning"><b><?=$r_surat_keluar;?></b></a> Surat Keluar yang dikembalikan untuk ditinjau ulang.</div>
	<?php endif; ?>
<?php endif; ?>