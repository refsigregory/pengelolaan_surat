    <div class="loginmodal-container">
     <h1>Aplikasi Pengelolaan Surat</h1><br>

  <?php
     if (!empty($this->session->flashdata('msg'))):
        $msg = $this->session->flashdata('msg');
  ?>
  <?php if($msg['type'] == 'success'): ?>
     <div class="alert alert-success"><?=$msg['message'];?></div>
  <?php elseif ($msg['type'] == 'warning'): ?>
     <div class="alert alert-warning"><?=$msg['message'];?></div>
  <?php elseif ($msg['type'] == 'error'): ?>
     <div class="alert alert-danger"><?=$msg['message'];?></div>
  <?php else: ?>
     <div class="alert alert-info"><?=$msg['message'];?></div>
  <?php endif; ?>
 <?php endif; ?>

     <form method="POST" action="<?=site_url('auth/checkLogin');?>">
        Nama Pengguna<br>
        <input type="text" name="username" >
        Kata Sandi<br>
        <input type="password" name="password">
        <input type="submit" name="login" class="login" value="Masuk">
     </form>
    </div>