<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Tambah Surat</b>
          </div> 
          <div class="panel-body"> <?=form_open_multipart('home/tambahSurat');?>
          <input type="hidden" name="type" value="<?=$type_surat;?>">
        <?php
            $error = $this->session->flashdata('error');
            if(isset($error)){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
        <?php } ?>

         <div class="form-group">
            <label>Nomor Surat:</label><br>
            <?php $id = $this->db->query("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'db_pengelolaan_surat' AND TABLE_NAME = 'tb_surat'")->result();?>
            <div class="row">
                <?php if($type_surat == "keluar"):?>
                    <div class="col-xs-12">
                        <input name="no_surat" class="form-control"/>
                    </div>
                <?php else:?>
                <div class="col-xs-2">
                    <input name="no_surat[]" class="form-control"/>
                </div>
                <div class="col-xs-2">
                    <input name="no_surat[]" class="form-control"/>
                </div>
                <div class="col-xs-2">
                    <input name="no_surat[]" class="form-control"/>
                </div>
                 <div class="col-xs-2">
                    <input name="no_surat[]" class="form-control"/>
                </div>
            <?php endif;?>
            </div>
        </div>  
        <div class="form-group">
            <label>Kategori:</label><br>
            <select name="id_kategori" class="form-control">
                <?php foreach($this->db->query("select * from tb_kategori")->result() as $row): ?>
                    <option value="<?=$row->id;?>"><?=$row->nama_kategori;?></option>
                <?php endforeach;?>
            </select>
        </div>  

        <div class="form-group">
            <label>File:</label><br>
            <input type="file" name="files" class="form-control" />
        </div>  

      <div class="form-group">
            <button class="btn btn-primary">Tambah</button>
      </div>
      </div>

</form>

</div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->